import 'package:mosc_obl_kandinsky/domain/create_map_point.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

Future<List<PlacemarkMapObject>> createPlacemarkMapObjects()async{
  final data = await createMapPoint();
  List<PlacemarkMapObject> result = [];
  for (int i = 0; i < data.length; i ++){
    result.add(PlacemarkMapObject(
        mapId: MapObjectId(data[i].name),
        point: Point(latitude: data[i].latitude, longitude: data[i].longitude),
      icon: PlacemarkIcon.single(
        PlacemarkIconStyle(
          image: BitmapDescriptor.fromAssetImage(
            'assets/blue.png'
          ),
          scale: 6
        )
      )
    ));
  }
  return result;
}