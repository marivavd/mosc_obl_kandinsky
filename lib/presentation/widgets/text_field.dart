import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomField extends StatefulWidget{
  final String label;
  final String hint;
  final TextEditingController controller;
  final bool enableObscure;
  final bool isValid;
  final Function(String)? onChange;

  const CustomField({super.key,
    required this.label,
    required this.hint,
    required this.controller,
    this.enableObscure = false,
    this.isValid = true,
    this.onChange});






  @override
  State<CustomField> createState() => CustomFieldState();

}

class CustomFieldState extends State<CustomField> {
  bool isObscure = true;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.label,
          style: Theme.of(context).textTheme.titleMedium,
        ),
        SizedBox(height: 8,),
        SizedBox(
          height: 44,
          child: TextField(
            controller: widget.controller,
            obscureText: (widget.enableObscure) ? isObscure : false,
            obscuringCharacter: '*',
            style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF3A3A3A)),
            onChanged: widget.onChange,
            decoration: InputDecoration(
              hintText: widget.hint,
              hintStyle: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFFCFCFCF)),
                enabledBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.enabledBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                focusedBorder: (widget.isValid) ? Theme.of(context).inputDecorationTheme.focusedBorder : Theme.of(context).inputDecorationTheme.errorBorder,
                contentPadding: const EdgeInsets.symmetric(vertical: 14, horizontal: 10),
                suffixIconConstraints: const BoxConstraints(minWidth: 34),
                suffixIcon: (widget.enableObscure)?
                GestureDetector(
                  onTap: (){
                    setState(() {
                      isObscure = !isObscure;
                    });
                  },
                  child: SvgPicture.asset(
                    (!isObscure)
                        ? "assets/eye.svg"
                        : "assets/eye-slash.svg",
                    // colorFilter: ColorFilter.mode(colors.iconTint, BlendMode.color)
                  ),
                )
                    :null


            ),
          ),
        )

      ],
    );
  }
}