import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';
import 'package:mosc_obl_kandinsky/main.dart';



class PlatformItem extends StatefulWidget {

  final ModelPlatform platform;
  final TextEditingController controller;
  final Function() on_change;
  final Function() remove;


  PlatformItem(
      {
        super.key,
        required this.platform,
        required this.controller,
        required this.on_change,
        required this.remove
      }
      );

  @override
  State<PlatformItem> createState() => _PlatformItemState();
}

class _PlatformItemState extends State<PlatformItem> {

  @override
  Widget build(BuildContext context) {
    var myapp = MyApp.of(context);
    var colors = myapp.getColors(context);
    return SizedBox(
        height: 91,
        child: Column(
            children: [
              Divider(height: 1, color: colors.subText,),
              Padding(
                padding: const EdgeInsets.all(14.0),
                child: Column(
                  children: [
                    Row(
                      children: [
                        InkWell(
                            onTap: (){
                              widget.remove();
                            },
                            child: SvgPicture.asset("assets/remove.svg", color: colors.iconTint,)
                        ),
                        const SizedBox(width: 14),
                        Image.network(widget.platform.icon, width: 30, height: 30),
                        const SizedBox(width: 12),
                        Expanded(
                          child: Text(widget.platform.title, style: TextStyle(
                              color: colors.text
                          )),
                        ),
                        Icon(Icons.info_outline_rounded, color: colors.text)
                      ],
                    ),
                    TextField(
                      controller: widget.controller,
                      style: TextStyle(
                          color: colors.text
                      ),
                      decoration: InputDecoration(
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: colors.subText
                          ),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: colors.text
                          ),
                        ),
                      ),
                      onChanged: (textChannels){
                        setState(() {
                          widget.platform.channels = textChannels;
                          widget.on_change();
                        });
                      },
                    )

                  ],
                ),
              )

            ])

    );
  }
}