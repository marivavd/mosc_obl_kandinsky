import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/presentation/theme/colors.dart';

var lightcolors = LightColorsApp();
var light = ThemeData(
  textTheme: TextTheme(
    titleLarge: TextStyle(
      fontSize: 24,
      height: 30/24,
      fontWeight: FontWeight.w500,
      color: lightcolors.text
    ),
    titleMedium: TextStyle(
        fontSize: 14,
        height: 16/14,
        fontWeight: FontWeight.w500,
        color: lightcolors.subText
    ),
    labelLarge: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w700,
        color: lightcolors.disableTextAccent
    ),
  ),
  filledButtonTheme: FilledButtonThemeData(
    style: FilledButton.styleFrom(
      disabledBackgroundColor: lightcolors.disableAccent,
      backgroundColor: lightcolors.accent,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4)
      )
    )
  ),
  inputDecorationTheme: InputDecorationTheme(
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: lightcolors.subText)
    ),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: lightcolors.subText)
    ),
    errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: lightcolors.error)
    ),

  )
);
var darkcolors = DarkColorsApp();
var dark = ThemeData(
    textTheme: TextTheme(
      titleLarge: TextStyle(
          fontSize: 24,
          height: 30/24,
          fontWeight: FontWeight.w500,
          color: darkcolors.text
      ),
      titleMedium: TextStyle(
          fontSize: 14,
          height: 16/14,
          fontWeight: FontWeight.w500,
          color: darkcolors.subText
      ),
      labelLarge: TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w700,
          color: darkcolors.textAccent
      ),
    ),
    filledButtonTheme: FilledButtonThemeData(
        style: FilledButton.styleFrom(
            disabledBackgroundColor: darkcolors.disableAccent,
            backgroundColor: darkcolors.accent,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(4)
            )
        )
    ),
    inputDecorationTheme: InputDecorationTheme(
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: darkcolors.subText)
      ),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: darkcolors.subText)
      ),
      errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: darkcolors.error)
      ),

    )
);