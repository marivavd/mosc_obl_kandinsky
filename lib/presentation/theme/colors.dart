import 'package:flutter/material.dart';

abstract class ColorsApp{
  abstract final Color text;
  abstract final Color accent;
  abstract final Color textAccent;
  abstract final Color disableAccent;
  abstract final Color disableTextAccent;
  abstract final Color hint;
  abstract final Color iconTint;
  abstract final Color background;
  abstract final Color error;
  abstract final Color block;
  abstract final Color subText;

}
class LightColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => Color(0xFF7576D6);

  @override
  // TODO: implement background
  Color get background => Color(0xFFFFFAF0);

  @override
  // TODO: implement block
  Color get block => Color(0xFFC4C8CB);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => Color(0xFFA7A7A7);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => Colors.white;

  @override
  // TODO: implement error
  Color get error => Color(0xFFCA0000);

  @override
  // TODO: implement hint
  Color get hint => Color(0xFFCFCFCF);

  @override
  // TODO: implement iconTint
  Color get iconTint => Color(0xFF141414);

  @override
  // TODO: implement subText
  Color get subText => Color(0xFF5C636A);

  @override
  // TODO: implement text
  Color get text => Color(0xFF3A3A3A);

  @override
  // TODO: implement textAccent
  Color get textAccent => Color(0xFFC4C8CB);

}

class DarkColorsApp extends ColorsApp{
  @override
  // TODO: implement accent
  Color get accent => Color(0xFF0B5ED7);

  @override
  // TODO: implement background
  Color get background => Color(0xFF1A1A1A);

  @override
  // TODO: implement block
  Color get block => Color(0xFF444648);

  @override
  // TODO: implement disableAccent
  Color get disableAccent => Color(0xFF575757);

  @override
  // TODO: implement disableTextAccent
  Color get disableTextAccent => Color(0xFF444648);

  @override
  // TODO: implement error
  Color get error => Color(0xFFCA0000);

  @override
  // TODO: implement hint
  Color get hint => Color(0xFF575757);

  @override
  // TODO: implement iconTint
  Color get iconTint => Color(0xFFFFFFFF);

  @override
  // TODO: implement subText
  Color get subText => Color(0xFF581212);

  @override
  // TODO: implement text
  Color get text => Color(0xFFFFFFFF);

  @override
  // TODO: implement textAccent
  Color get textAccent => Color(0xFFFFFFFF);

}