import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class MapPage extends StatefulWidget {
  const MapPage({super.key});


  @override
  State<MapPage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MapPage> {
  Completer<YandexMapController> completer = Completer();
  List<PlacemarkMapObject> placemarkobj = [];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp)async {

    });

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: YandexMap(
        onMapCreated: onMapCreated,
        mapObjects: placemarkobj,
      ),
    );
  }

  Future<void> onMapCreated(YandexMapController controller)async {
    final placemarkobjOld = await createPlacemarkMapObjects();
    setState(() {
      placemarkobj = placemarkobjOld;
    });
    completer.complete(controller);
  }
}
