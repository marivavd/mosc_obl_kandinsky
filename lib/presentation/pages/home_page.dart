import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_new.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/create_profile.dart';
import 'package:mosc_obl_kandinsky/domain/log_out_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/make_card_news.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/article_page.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/profile.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class HomePage extends StatefulWidget {
  HomePage({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<HomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<HomePage> {
  List<ModelNews> news = [];
  List<ModelNews> newsMedia = [];
  int current = 0;
  int column = 0;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      news = await createListofNews();
      for(int i = 0; i <news.length; i++){
        if (news[i].type_media != 'none'){
          newsMedia.add(news[i]);
        }
      }
      setState(() {

      });
    });
    subsribeNews((modelNews)=>{
      setState(() {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
          final newsold = await createListofNews();

          setState(() {
            news = newsold;
            newsMedia.clear();
            for (int i = 0; i < news.length; i ++){
              if (news[i].type_media != 'none'){
                newsMedia.add(news[i]);
              }
            }
          });
        });
      })
    });


  }

  void changeLayout(int newIndex){
    setState(() {
      current = newIndex;
    });
    Navigator.pop(context);
    showSetka();
  }


  void showSetka(){
    var colors = MyApp.of(context).getColors(context);
    showDialog(context: context, builder: (_) => Dialog(
      surfaceTintColor: Colors.transparent,
      insetPadding: const EdgeInsets.symmetric(horizontal: 22),
      backgroundColor: colors.background,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12))
      ),
      child: SizedBox(
        width: double.infinity,
        height: 248,
        child: Padding(
          padding: const EdgeInsets.only(top: 18, bottom: 28, left: 22, right: 8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("СЕТКА ОТОБРАЖЕНИЯ", style: TextStyle(
                color: colors.subText,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              )),
              const SizedBox(height: 16),
              Divider(height: 1, color: colors.subText, endIndent: 2),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(0);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model0${(current == 0) ? "check" : ""}.svg",),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Card", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (current == 0) ? FontWeight.w700 : FontWeight.w400
                      )),
                    ),
                    (current == 0) ? SvgPicture.asset("assets/Vector.svg", color: colors.iconTint,) : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(1);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model1${(current == 1) ? "check" : ""}.svg"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Text", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (current == 1) ? FontWeight.w700: FontWeight.w400
                      )),
                    ),
                    (current == 1) ? SvgPicture.asset("assets/Vector.svg", color: colors.iconTint,) : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
              const SizedBox(height: 28),
              GestureDetector(
                onTap: (){
                  changeLayout(2);
                },
                child: Row(
                  children: [
                    SvgPicture.asset("assets/model2${(current == 2) ? "check" : ""}.svg"),
                    const SizedBox(width: 18),
                    Expanded(
                      child: Text("Media", style: TextStyle(
                          color: colors.text,
                          fontSize: 18,
                          fontWeight: (current == 2) ? FontWeight.w700 : FontWeight.w400
                      )),
                    ),
                    (current == 2) ? SvgPicture.asset("assets/Vector.svg") : const SizedBox(),
                    const SizedBox(width: 22),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    ));

  }




  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    var width = MediaQuery.of(context).size.width;

    return (news.length != 0)?Scaffold(
      backgroundColor: colors.background,
      body: CustomScrollView(
        slivers: [
            SliverToBoxAdapter(
      child:Padding(
              padding: EdgeInsets.symmetric(horizontal: 22),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 63,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Лента новостей',
                      style: TextStyle(
                        fontSize: 24,
                        color: colors.text,
                        fontWeight: FontWeight.w700,
                        height: 16/24
                      ),),
                      InkWell(
                        onTap: (){
                          showSetka();
                        },
                        child: SvgPicture.asset('assets/setka$current.svg', color: colors.iconTint, width: 22, height: 22,),
                      )
                    ],
                  ),
                  SizedBox(height: 18,)
                ],
              ),
            ),),
          (current != 2)?SliverList.builder(
            itemCount: news.length,
              itemBuilder: (_, index){
                return Padding(
                    padding: EdgeInsets.symmetric(horizontal: 22),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 12,),
                      Row(
                        children: [
                          Image.network(news[index].platform.icon, height: 30, width: 30,),
                          SizedBox(width: 12,),
                          Column(
                            children: [
                              Row(
                                children: [Text(
                                  '${news[index].platform.title} • ',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),

                                ),
                                  Text(
                                      news[index].platform.channels,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: colors.text,
                                      height: 16/12
                                    ),
                                  )
                                ]

                              ),
                              Text(
                                news[index].published,
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    color: colors.text,
                                  height: 16/10
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 12,),
                      (current == 0 && news[index].type_media != 'none')?
                      InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                            }));
                          },
                          child: SizedBox(
                            width: width - 48,
                            child: Row(

                              children: [
                                Expanded(child: Text(
                                  (news[index].title.length > 250)?
                                  "${news[index].title.substring(0, 250)}..."
                                      :news[index].title,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500,
                                      color: colors.text
                                  ),
                                )),
                                (news[index].type_media == 'photo')?
                                Expanded(child: AspectRatio(child: Image.network(news[index].media), aspectRatio: 4/3,)):
                                (news[index].type_media == 'link')?Container(
                                        color: colors.block,
                                        width: 100, height: 75,
                                        child: Image.asset('assets/link.png')
                                      ):
                                Container(
                                    color: colors.block,
                                    width: 100, height: 75,
                                    child: Image.asset('assets/audio1.png')
                                  ),
                              ],
                            ),
                          ))
                          :(current == 1)?
                      InkWell(
                        child: Text(
                          (news[index].title.length > 500)?"${news[index].title.substring(0, 500)}...":news[index].title,
                          style: TextStyle(
                              color: Colors.black,
                              height: 80/70,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                          }));
                        },
                      ):InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                          }));
                        },
                        child: Text(
                          (news[index].title.length > 250)?"${news[index].title.substring(0, 250)}...":news[index].title,
                          style: TextStyle(
                              color: Colors.black,
                              height: 80/70,
                              fontSize: 14,
                              fontWeight: FontWeight.w500
                          ),
                        ),
                      ),
                      SizedBox(height: 12,),
                      InkWell(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                          }));
                        },
                        child: (current == 0)?Text(
                          (news[index].text.length > 500)?"${news[index].text.substring(0, 500)}...":news[index].text,
                          style: TextStyle(
                              color: colors.text,
                              height: 112/96,
                              fontSize: 12,
                              fontWeight: FontWeight.w500
                          ),
                        ):Text(
                          (news[index].text.length > 1500)?"${news[index].text.substring(0, 1500)}...":news[index].text,
                          style: TextStyle(
                              color: colors.text,
                              height: 112/96,
                              fontSize: 12,
                              fontWeight: FontWeight.w500
                          ),
                        )

                  ),
                      SizedBox(height: 12,),
                      Divider(height: 1, color: colors.text,),
                      SizedBox(height: 12,)
                ]
                ));
              }):
             SliverPadding(padding: EdgeInsets.symmetric(horizontal: 4),
            sliver: SliverGrid.count(crossAxisCount:  2,
              crossAxisSpacing: 4,
              mainAxisSpacing: 4,

              children: List.generate(newsMedia.length, (index) {
                return InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => ArticlePage(news: news[index]))).then((value) => setState(() {

                      }));
                    },
                    child:
                    (newsMedia[index].type_media == 'photo')?Container(
                        decoration: BoxDecoration(

                          image: DecorationImage(
                            image:NetworkImage(newsMedia[index].media),
                            fit: BoxFit.cover,

                          ),
                        )
                    ):Container(
                      color: colors.block,
                    )

                );
              },),
          ))
        ],
      ),

    ):Center(child: CircularProgressIndicator(),);
  }
}
