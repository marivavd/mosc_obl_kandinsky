import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/data/models/model_ai.dart';
import 'package:mosc_obl_kandinsky/data/models/model_style.dart';
import 'package:mosc_obl_kandinsky/domain/kandinsky_use_case.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Kandinsky extends StatefulWidget {
  const Kandinsky({super.key});


  @override
  State<Kandinsky> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Kandinsky> {
  Uint8List? image;

  Map<String, double> rations = {
    "1 : 1": 1/1,
    "2 : 3": 2/3,
    "3 : 2": 3/2,
    "9 : 16": 9/16,
    "16 : 9" : 16/9
  };

  late MapEntry<String, double> ratio;
  ModelStyle? style;
  ModelAi? modelAI;

  var controller = TextEditingController(text: 'theater');

  List<ModelStyle> styles = [];
  bool isCensured = false;

  KandinskyUseCase useCase = KandinskyUseCase();

  @override
  void initState() {
    super.initState();
    ratio = rations.entries.first;
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      showLoading(context);

      await useCase.getStyles(
              (styles) {
            setState(() {
              this.styles = styles;
              style = this.styles.first;
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );

      await useCase.getModelAI(
              (model) {
            setState(() {
              modelAI = model;
              hideLoading(context);
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );
    });
  }


  Future<void> pressButtonGenerate() async {
    useCase.pressButton(
        ratio.value,
        controller.text,
        modelAI,
        style,
        onInit: (id){
          setState(() {
            isCensured = false;
            image = null;
          });
          showLoading(context);
        },
        onCheckStatus: (status) {

        },
        onDone: (image){
          setState(() {
            this.image = image;
          });
          hideLoading(context);
        },
        onCensured: (_) {
          setState(() {
            isCensured = true;
          });
          hideLoading(context);
        },
        onError: (error) {
          hideLoading(context);
          showError(context, error);
        }
    );
  }




  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Center(
        child: Column(
          children: [
            (image != null)?Image.memory(image!, height: 200, width: 200,):Container(height: 200, width: 200,),
            OutlinedButton(onPressed: ()async{await pressButtonGenerate();}, child: Text('Generate'))
          ],
        ),
      ),
    );

}}
