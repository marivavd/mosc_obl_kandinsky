
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_new.dart';
import 'package:mosc_obl_kandinsky/main.dart';

class Media extends StatefulWidget {
  Media({super.key, required this.news});
  ModelNews news;


  @override
  State<Media> createState() => _MediaState();
}

class _MediaState extends State<Media>{


  Widget getBodyPortrait(){
    var colors = MyApp.of(context).getColors(context);
    return Column(
      children: [
        const SizedBox(height: 57),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                SizedBox(width: 22,),
                InkWell(
                  child: SvgPicture.asset('assets/close.svg', color: colors.iconTint, height: 18, width: 18,),
                  onTap: (){Navigator.of(context).pop();},
                ),
                SizedBox(width: 15,),
                Image.network(widget.news.platform.icon, height: 30, width: 30,),
                SizedBox(width: 12,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.news.platform.title,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: colors.text
                      ),
                    ),
                    Text(
                      widget.news.published,
                      style: TextStyle(
                          fontSize: 10,
                          fontWeight: FontWeight.w400,
                          color: colors.text
                      ),
                    )
                  ],
                )
              ],
            ),
            Row(
              children: [
                SvgPicture.asset('assets/share.svg', color: colors.iconTint,),
                SizedBox(width: 14,),
                SvgPicture.asset('assets/tofavourite.svg', color: colors.iconTint,),
              ],
            ),
            SizedBox(height: 22,)

          ],
        ),
        Padding(
          padding: const EdgeInsets.only(top: 22, left: 22, right: 22),
          child: Text(widget.news.title, maxLines: 1, style: TextStyle(
            fontSize:  16,
            overflow: TextOverflow.ellipsis,
            color: colors.text,
          )),
        ),
        Expanded(child: Image.network(widget.news.media)),
        Padding(
          padding: const EdgeInsets.all(22),
          child: Text(
              widget.news.text,
              maxLines: 5,
              style: TextStyle(fontSize: 12, color: colors.subText, overflow: TextOverflow.ellipsis)
          ),
        )
      ],
    );
  }

  Widget getBodyLandscape(){
    var colors = MyApp.of(context).getColors(context);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(width: 22),
        Padding(
          padding: const EdgeInsets.only(top: 22),
          child: GestureDetector(
              onTap: (){
                Navigator.of(context).pop();
              },
            child: SvgPicture.asset('assets/close.svg', color: colors.iconTint, height: 18, width: 18,),
          ),
        ),
        Expanded(child: Image.network(widget.news.media)),
        Padding(
          padding: EdgeInsets.only(top: 22),
          child: Row(
            children: [
              SvgPicture.asset('assets/share.svg', color: colors.iconTint,),
              SizedBox(width: 14,),
              SvgPicture.asset('assets/tofavourite.svg', color: colors.iconTint,),
              SizedBox(width: 22),
            ],
          ),
        ),
      ],
    );
  }








  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);
    var isLandscape = MediaQuery.of(context).orientation == Orientation.landscape;

    return Scaffold(
        backgroundColor: colors.background,
        body: (isLandscape) ? getBodyLandscape() : getBodyPortrait()

    );
  }
}

