import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/forgot_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/otp.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_up.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ForgotPass extends StatefulWidget {
  const ForgotPass({super.key});


  @override
  State<ForgotPass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ForgotPass> {
  var email =TextEditingController();
  bool isRedEmail = false;
  bool isValid = false;
  ForgotUseCase useCase = ForgotUseCase();

  void onChange(_){
    setState(() {
      isRedEmail = email.text.isNotEmpty && !checkEmail(email.text);
      isValid = !isRedEmail;
    });


  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                'Восстановление пароля',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Введите свою почту',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 28,),
              CustomField(label: 'Почта', hint: '***********@mail.com', controller: email, isValid: !isRedEmail, onChange: onChange,),
              SizedBox(height: 503,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      useCase.pressButton(email.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => OTPpage(email: email.text))).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            await showError(context, e);
                          });
                    }:null,
                    child: Text(
                      'Отправить код',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Я вспомнил свой пароль! ',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        'Вернуться',
                        style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                        }));

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),

    );
  }
}
