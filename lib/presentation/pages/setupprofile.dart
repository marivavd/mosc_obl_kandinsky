import 'dart:async';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mosc_obl_kandinsky/data/models/model_ai.dart';
import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/data/models/model_style.dart';
import 'package:mosc_obl_kandinsky/domain/avatar_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/create_profile.dart';
import 'package:mosc_obl_kandinsky/domain/kandinsky_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/log_out_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/make_all_platforms.dart';
import 'package:mosc_obl_kandinsky/domain/prepare_to_ipdate.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/profile.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/theme/theme.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/platform_item.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SetupProfile extends StatefulWidget {
  SetupProfile({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<SetupProfile> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SetupProfile> {
  late AvatarUseCase useCase;
  List<ModelPlatform> all_platforms = [];
  List<ModelPlatform> my_platforms = [];

  var fullname = TextEditingController();
  var phone = TextEditingController();
  var birthday = TextEditingController();
  Map<String, double> rations = {
    "1 : 1": 1/1,
    "2 : 3": 2/3,
    "3 : 2": 3/2,
    "9 : 16": 9/16,
    "16 : 9" : 16/9
  };
  late MapEntry<String, double> ratio;
  ModelStyle? style;
  ModelAi? modelAI;
  List<ModelStyle> styles = [];
  bool isCensured = false;

  KandinskyUseCase KanduseCase = KandinskyUseCase();



  void onPickAvatar(Uint8List bytes){
    setState(() {
      widget.profile.avatar = bytes;
    });
  }

  void onRemoveAvatar(){
    setState(() {
      widget.profile.avatar = null;
    });
  }

  Future<ImageSource?> onChooseSource() async {

    ImageSource? imageSource;

    await showDialog(context: context, builder: (_) => AlertDialog(
      title: const Text("Выберите источник"),
      actions: [
        TextButton(onPressed: (){
          imageSource = ImageSource.camera;
          Navigator.pop(context);
        }, child: const Text("Камера")),
        TextButton(onPressed: (){
          imageSource = ImageSource.gallery;
          Navigator.pop(context);
        }, child: const Text("Галлерея")),
        TextButton(onPressed: (){
          kandinsky();
          Navigator.pop(context);
        }, child: const Text("Кандинский")),
      ],
    ));

    return imageSource;
  }

  Future<void> kandinsky()async{
    KanduseCase.pressButton(
        ratio.value,
        fullname.text,
        modelAI,
        style,
        onInit: (id){
          setState(() {
            isCensured = false;
            widget.profile.avatar = null;
          });
          showLoading(context);
        },
        onCheckStatus: (status) {

        },
        onDone: (image){
          setState(() {
            widget.profile.avatar = image;
          });
          hideLoading(context);
        },
        onCensured: (_) {
          setState(() {
            isCensured = true;
          });
          hideLoading(context);
        },
        onError: (error) {
          hideLoading(context);
          showError(context, error);
        }
    );


  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fullname.text = widget.profile.fullname;
    phone.text = widget.profile.phone;
    birthday.text = widget.profile.birthday;
    make_all_platforms(widget.profile.platforms).then((value) => all_platforms = value);
    useCase = AvatarUseCase(onChooseSource: onChooseSource, onPickAvatar: onPickAvatar, onRemoveAvatar: onRemoveAvatar);
    setState(() {

    });
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      ratio = rations.entries.first;
      showLoading(context);

      await KanduseCase.getStyles(
              (styles) {
            setState(() {
              this.styles = styles;
              style = this.styles.first;
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );

      await KanduseCase.getModelAI(
              (model) {
            setState(() {
              modelAI = model;
              hideLoading(context);
            });
          },
              (error) {
            hideLoading(context);
            showError(context, error);
          }
      );
    });
  }





  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 73,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14),
                              side: BorderSide(
                                  color: colors.accent,
                                  width: 1
                              )
                          ),
                          minimumSize: Size.zero,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12,
                              horizontal: 12
                          )
                      ),
                      onPressed: (){
                        useCase.pressButton((widget.profile.avatar != null) ? true : false);
                      },
                      child: Icon((widget.profile.avatar != null) ? Icons.restore_from_trash: Icons.add_photo_alternate_outlined, color: colors.accent,),),

                    ClipRRect(
                      borderRadius: BorderRadius.circular(32),
                      child: (widget.profile.avatar != null)?Container(
                        height: 151,
                        width: 151,
                        child: Image.memory(widget.profile.avatar, fit: BoxFit.cover,),

                      ): Image.asset("assets/Group 4.png", height: 151, width: 151,),
                    ),
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(14),
                              side: BorderSide(
                                  color: colors.accent,
                                  width: 1
                              )
                          ),
                          minimumSize: Size.zero,
                          padding: const EdgeInsets.symmetric(
                              vertical: 12,
                              horizontal: 12
                          )
                      ),
                      onPressed: (){
                        MyApp.of(context).changeDiffTheme(context);
                      },
                      child: (colors == lightcolors) ? SvgPicture.asset('assets/sun.svg', color: colors.accent):SvgPicture.asset('assets/moon.svg', color: colors.accent)),

                  ],
                ),
                SizedBox(height: 24,),
                CustomField(label: 'ФИО', hint: 'Введите ваше ФИО', controller: fullname),
                SizedBox(height: 24,),
                CustomField(label: 'Телефон', hint: '+7 (000) 000 00 00', controller: fullname),
                SizedBox(height: 24,),
                CustomField(label: 'Дата рождения', hint: 'Выберите дату', controller: fullname),
                SizedBox(height: 24,),
                Text(
                  "Источники новостей",
                  style: Theme.of(context).textTheme.titleSmall,
                ),
                SizedBox(height: 8,)

              ],
            ),
          ),),
            SliverToBoxAdapter(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 24),
                child: Container(
                  width: MediaQuery.of(context).size.width - 48,
                  height: 44 + (widget.profile.platforms.length * 91),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      border:
                      Border.all(color: Color(0xFF5C636A), width: 1)),
                  child: Column(
                    children: [
                      Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 14,
                                  vertical: 14
                              ),
                              child: Row(
                                  children: [
                                    Expanded(
                                      child: Text(
                                          "Выберите платформы",
                                          style: TextStyle(
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500,
                                              color: colors.text
                                          )
                                      ),
                                    ),
                                    GestureDetector(
                                        onTap: ()async{
                                          all_platforms = await make_all_platforms(widget.profile.platforms);
                                          //Navigator.push(context, MaterialPageRoute(builder: (context) => Choose_Platform(profile: widget.profile, all_platforms: all_platforms))).then((value) => setState(() {}));
                                        },
                                        child: Icon(Icons.add, color: colors.iconTint, size: 14)
                                    )
                                  ]
                              ),
                            ),
                          ]
                      ),

                      SizedBox(
                        height: 91 * widget.profile.platforms.length.toDouble(),
                        child: ListView.builder(
                            itemCount: widget.profile.platforms.length,
                            itemBuilder: (_, index){
                              List<TextEditingController> controllers = make_controllers(widget.profile.platforms);
                              return PlatformItem(platform: widget.profile.platforms[index], controller: controllers[index], on_change: (){
                                setState(() {
                                  widget.profile.platforms[index].channels = controllers[index].text;
                                });
                              }, remove: (){
                                setState(() {
                                  widget.profile.platforms.remove(widget.profile.platforms[index]);
                                });
                              });
                            }),
                      )
                    ],
                  ),
                ),
              ),
            ),




            SliverToBoxAdapter(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 24,),
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          height: 46,
                          width: 342,
                          child: FilledButton(
                            style: FilledButton.styleFrom(
                                backgroundColor: colors.accent,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4)
                                )),
                            child: Text(
                              "Сохранить",
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                            onPressed: ()async{

                                await prepare_to_update(profile: widget.profile, fullname_controller: fullname, phone_controller: phone, birthday_controller: birthday, context: context);

                                Navigator.push(context, MaterialPageRoute(builder: (context) => Profile(profile: widget.profile,))).then((value) => setState(() {}));



                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 18,),

                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          width: 342,
                          height: 46,
                          child: FilledButton(
                            style: FilledButton.styleFrom(
                                backgroundColor: Colors.transparent,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                    side: BorderSide(
                                        width: 1,
                                        color: colors.accent
                                    )
                                )
                            ),
                            onPressed: ()async{
                              widget.profile = await createProfile();
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Profile(profile: widget.profile))).then((value) => setState(() {}));
                            },
                            child: Text(
                              "Отменить",
                              style: Theme.of(context).textTheme.titleLarge!.copyWith(color: colors.accent),
                            ),

                          ),
                        ),
                      ),
                      SizedBox(height: 32,)
                    ],
                  ),
                )
            ),

          ]
        )
    );
  }
}
