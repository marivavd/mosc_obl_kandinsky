import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/forgot_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/otp_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/change_pass.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_up.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class OTPpage extends StatefulWidget {OTPpage({super.key, required this.email});
  String email;


  @override
  State<OTPpage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<OTPpage> {
  ForgotUseCase useCaseRepeat = ForgotUseCase();
  OTPUseCase useCase = OTPUseCase();
  var controller = TextEditingController();

  bool isValid = false;


  void onChange(_){
    setState(() {

      isValid = controller.text.length == 6;
    });
  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                'Верификация',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Введите 6-ти значный код из письма ',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 58,),
              CustomField(label: "", hint: "", controller: controller, onChange: onChange,),
              SizedBox(height: 48,),
              Align(
                alignment: Alignment.center,
                child: InkWell(
                  child: Text(
                    'Получить новый код',
                    style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w500, color: Color(0xFF7576D6)),
                  ),
                  onTap: (){
                    showLoading(context);
                    useCaseRepeat.pressButton(widget.email,
                            (_){
                          hideLoading(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context) => OTPpage(email: widget.email,))).then((value) => setState(() {}));
                        },
                            (String e)async{
                          hideLoading(context);
                          await showError(context, e);
                        });

                  },
                ),
              ),
              SizedBox(height: 449,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      useCase.pressButton(widget.email, controller.text,
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => ChangePass())).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            await showError(context, e);
                          });
                    }:null,
                    child: Text(
                      'Сбросить пароль',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Я вспомнил свой пароль! ',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        'Вернуться',
                        style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                        }));

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),

    );
  }
}
