import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});


  @override
  State<SignUp> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignUp> {
  SignUpUseCase useCase = SignUpUseCase();
  var email =TextEditingController();
  var password = PasswordConroller();
  var confirmPassword = PasswordConroller();
  bool isRedEmail = false;
  bool isValid = false;

  void onChange(_){
    setState(() {
      isRedEmail = email.text.isNotEmpty && !checkEmail(email.text);
      isValid = !isRedEmail && password.text.isNotEmpty && confirmPassword.text.isNotEmpty;
    });


  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                'Создать аккаунт',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Завершите регистрацию чтобы начать',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 28,),
              CustomField(label: 'Почта', hint: '***********@mail.com', controller: email, isValid: !isRedEmail, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Пароль', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Повторите пароль', hint: '**********', controller: confirmPassword, onChange: onChange, enableObscure: true,),
              SizedBox(height: 319,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      useCase.pressButton(email.text, password.hashPass(),
                          confirmPassword.hashPass(),
                          (_){
                        hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {}));
                          },
                          (String e)async{
                        hideLoading(context);
                        await showError(context, e);
                          });

                    }:null,
                    child: Text(
                      'Зарегистрироваться',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'У меня уже есть аккаунт! ',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        'Войти',
                        style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                        }));

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),

    );
  }
}
