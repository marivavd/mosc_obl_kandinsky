import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/forgot_pass.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/holder.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_up.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class SignIn extends StatefulWidget {
  const SignIn({super.key});


  @override
  State<SignIn> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<SignIn> {
  var email =TextEditingController();
  var password = PasswordConroller();
  bool isRedEmail = false;
  bool isValid = false;
  bool rememberPassword = false;
  SignInUseCase useCase = SignInUseCase();

  void onChange(_){
    setState(() {
      isRedEmail = email.text.isNotEmpty && !checkEmail(email.text);
      isValid = !isRedEmail && password.text.isNotEmpty;
    });


  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                'Добро пожаловать',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Заполните почту и пароль чтобы продолжить',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 28,),
              CustomField(label: 'Почта', hint: '***********@mail.com', controller: email, isValid: !isRedEmail, onChange: onChange,),
              SizedBox(height: 24,),
              CustomField(label: 'Пароль', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 18,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 22,
                        width: 22,
                        child: Checkbox(
                          value: rememberPassword,
                          onChanged: (value){
                            setState(() {
                              rememberPassword = value!;
                            });
                          },
                          side: BorderSide(
                            width: 1,
                            color: Color(0xFF5C636A),

                          ),
                          activeColor: Color(0xFF7576D6),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)
                          ),
                        ),
                      ),
                      SizedBox(width: 8,),
                      Text(
                        "Запомнить меня",
                        style: Theme.of(context).textTheme.titleMedium,
                      ),

                    ],
                  ),
                  InkWell(
                    onTap: (){ Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPass())).then((value) => setState(() {
                    }));},
                    child: Text('Забыли пароль?',style: Theme.of(context).textTheme.titleMedium!.copyWith(color: Color(0xFF7576D6),
                    )
                    ),
                  )
                ],
              ),
              SizedBox(height: 371,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      useCase.pressButton(email.text, password.hashPass(),
                               (_){
                        hideLoading(context);
                        Navigator.push(context, MaterialPageRoute(builder: (context) => Home())).then((value) => setState(() {}));
                      },
                      (String e)async{
                      hideLoading(context);
                      await showError(context, e);
                      });
                    }:null,
                    child: Text(
                      'Войти',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'У меня нет аккаунта! ',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        'Создать',
                        style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp())).then((value) => setState(() {

                        }));

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),

    );
  }
}
