import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_new.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/create_profile.dart';
import 'package:mosc_obl_kandinsky/domain/log_out_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/media.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/setupprofile.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ArticlePage extends StatefulWidget {
  ArticlePage({super.key, required this.news});
  ModelNews news;


  @override
  State<ArticlePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ArticlePage> {
  LogOutUseCase useCase = LogOutUseCase();





  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
        backgroundColor: colors.background,
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height:63,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        InkWell(
                          child: SvgPicture.asset('assets/back.svg', color: colors.iconTint,),
                          onTap: (){
                            Navigator.of(context).pop();
                          },
                        ),
                        SizedBox(width: 14,),
                        Image.network(widget.news.platform.icon, height: 30, width: 30,),
                        SizedBox(width: 12,),
                        Column(
                          children: [
                            Row(
                                children: [Text(
                                  '${widget.news.platform.title} • ',
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w400,
                                      color: colors.text
                                  ),

                                ),
                                  Text(
                                    widget.news.platform.channels,
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w400,
                                        color: colors.text,
                                        height: 16/12
                                    ),
                                  )
                                ]

                            ),
                            Text(
                              widget.news.published,
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400,
                                  color: colors.text,
                                  height: 16/10
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    Row(
                      children: [
                        SvgPicture.asset('assets/share.svg', color: colors.iconTint,),
                        SizedBox(width: 14,),
                        SvgPicture.asset('assets/tofavourite.svg', color: colors.iconTint,),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 18,),
                Text(
                  widget.news.title,
                  style: TextStyle(
                    color: colors.text,
                    fontSize: 18,
                    fontWeight: FontWeight.w700,
                    height: 64/72
                  ),
                ),
                SizedBox(height: 18,),
                (widget.news.type_media == 'none')?
                Text(
                  widget.news.text,
                  style: TextStyle(
                      color: colors.text,
                      height: 196/210,
                      fontSize: 14,
                      fontWeight: FontWeight.w500
                  ),
                ):(widget.news.type_media == 'link')?
                Column(
                  children: [
                    Text(
                      widget.news.media,
                      style: TextStyle(
                          color: colors.accent,
                          height: 196/210,
                          fontSize: 14,
                          fontWeight: FontWeight.w500
                      ),
                    ),
                    SizedBox(height: 18,),
                    Text(
                      widget.news.text,
                      style: TextStyle(
                          color: colors.text,
                          height: 196/210,
                          fontSize: 14,
                          fontWeight: FontWeight.w500
                      ),
                    )
                  ],
                ):(widget.news.type_media == 'audio')?Column(
                  children: [
                    Image.asset('assets/audio.png'),

                    SizedBox(height: 18,),
                    Text(
                      widget.news.text,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: colors.text
                      ),)
                  ],
                ):Column(
                  children: [
                    InkWell(
                      onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => Media(news: widget.news))).then((value) => setState(() {

                      }));
                      },
                      child: Image.network(widget.news.media),
                    ),

                    SizedBox(height: 18,),
                    Text(
                      widget.news.text,
                      style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: colors.text
                      ),)
                  ],
                )





              ],
            ),
          ),
        )
    );
  }
}
