import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/domain/change_pass_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class ChangePass extends StatefulWidget {
  const ChangePass({super.key});


  @override
  State<ChangePass> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<ChangePass> {
  ChangePassUseCase useCase = ChangePassUseCase();
  var password = PasswordConroller();
  var confirmPassword = PasswordConroller();
  bool isValid = false;

  void onChange(_){
    setState(() {
      isValid = password.text.isNotEmpty && confirmPassword.text.isNotEmpty;
    });


  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 83,),
              Text(
                'Новый пароль',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              SizedBox(height: 8,),
              Text(
                'Введите новый пароль',
                style: Theme.of(context).textTheme.titleMedium,
              ),
              SizedBox(height: 28,),
              CustomField(label: 'Пароль', hint: '**********', controller: password, onChange: onChange, enableObscure: true,),
              SizedBox(height: 24,),
              CustomField(label: 'Повторите пароль', hint: '**********', controller: confirmPassword, onChange: onChange, enableObscure: true,),
              SizedBox(height: 411,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  height: 46,
                  width: 342,
                  child: FilledButton(
                    onPressed: (isValid)?(){
                      showLoading(context);
                      showLoading(context);
                      useCase.pressButton(password.hashPass(),
                          confirmPassword.hashPass(),
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            await showError(context, e);
                          });

                    }:null,
                    child: Text(
                      'Подтвердить',
                      style: Theme.of(context).textTheme.labelLarge,
                    ),
                    style: Theme.of(context).filledButtonTheme.style,
                  ),
                ),
              ),
              SizedBox(height: 14,),
              Align(
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Я вспомнил свой пароль! ',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w400),
                    ),
                    InkWell(
                      child: Text(
                        'Вернуться',
                        style: Theme.of(context).textTheme.titleMedium!.copyWith(fontWeight: FontWeight.w700, color: Color(0xFF7576D6)),
                      ),
                      onTap: (){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {

                        }));

                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),

    );
  }
}
