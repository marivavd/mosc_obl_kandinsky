import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/create_profile.dart';
import 'package:mosc_obl_kandinsky/domain/log_out_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/home_page.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/profile.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Home extends StatefulWidget {
  const Home({super.key});


  @override
  State<Home> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Home> {
  int index = 0;
  ModelProfile? profile;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async{
      profile = await createProfile();
      setState(() {

      });
    });
  }




  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return (profile != null)?Scaffold(
      backgroundColor: colors.background,
      body: [HomePage(profile: profile!,), Scaffold(backgroundColor: colors.background,), Scaffold(backgroundColor: colors.background,),Profile(profile: profile!),][index],
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: colors.background,
        onTap: (val){
          setState(() {
            index = val;
          });
        },
        currentIndex: index,
        type: BottomNavigationBarType.fixed,
        selectedLabelStyle: TextStyle(
            color: colors.accent,
            fontWeight: FontWeight.w400,
            fontSize: 12
        ),
        unselectedLabelStyle: TextStyle(
            color: colors.text,
            fontWeight: FontWeight.w400,
            fontSize: 12
        ),
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedFontSize: 12,
        selectedItemColor: colors.accent,
        unselectedFontSize: 12,
        iconSize: 24,
        unselectedItemColor: colors.text,

        items: [
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/home.svg', color: (index == 0)?colors.accent:colors.text), label: 'Home'),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/search.svg', color: (index == 1)?colors.accent:colors.text), label: 'Search'),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/favourite.svg', color: (index == 2)?colors.accent:colors.text), label: 'Favourite'),
          BottomNavigationBarItem(icon: SvgPicture.asset('assets/profile-circle.svg', color: (index == 3)?colors.accent:colors.text), label: 'Profile'),
        ],
      ),
    ):Center(child: CircularProgressIndicator(),);
  }
}
