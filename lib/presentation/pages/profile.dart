import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/domain/controllers/password_controller.dart';
import 'package:mosc_obl_kandinsky/domain/create_profile.dart';
import 'package:mosc_obl_kandinsky/domain/log_out_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_in_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/sign_up_use_case.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:mosc_obl_kandinsky/presentation/create_place_map_objects.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/setupprofile.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:mosc_obl_kandinsky/presentation/widgets/text_field.dart';
import 'package:yandex_mapkit/yandex_mapkit.dart';

class Profile extends StatefulWidget {
  Profile({super.key, required this.profile});
  ModelProfile profile;


  @override
  State<Profile> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<Profile> {
  LogOutUseCase useCase = LogOutUseCase();





  @override
  Widget build(BuildContext context) {
    var colors = MyApp.of(context).getColors(context);

    return Scaffold(
      backgroundColor: colors.background,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 73,),
              Row(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(32),
                    child: (widget.profile.avatar != null)? Container(width: 121, height: 121, child: Image.memory(widget.profile.avatar, fit: BoxFit.cover) ):Image.asset("assets/Group 4.png", height: 121, width: 121),
                  ),
                  SizedBox(width: 48,),
                  Flexible(child: Text(
                    widget.profile.fullname,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: colors.text
                    ),
                  ))
                ],
              ),
              SizedBox(height: 28,),
              Divider(height: 1, color: colors.subText),
              SizedBox(height: 24,),
              Text(
                "История",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Прочитанные статьи",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Чёрный список",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 24,),
              Text(
                "Настройки",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              InkWell(
                child: Text(
                  "Редактирование профиля",
                  style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: colors.text
                  ),
                ),
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => SetupProfile(profile: widget.profile))).then((value) => setState(() {}));

                },
              ),
              SizedBox(height: 18,),
              Text(
                "Политика конфиденциальности",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "Оффлайн чтение",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 18,),
              Text(
                "О нас",
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: colors.text
                ),
              ),
              SizedBox(height: 24,),
              Align(
                alignment: Alignment.center,
                child: SizedBox(
                  width: 342,
                  height: 46,
                  child: FilledButton(
                    style: FilledButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4),
                            side: BorderSide(
                                width: 1,
                                color: colors.error
                            )
                        )
                    ),
                    onPressed: ()async{
                      showLoading(context);
                      useCase.pressButton(
                              (_){
                            hideLoading(context);
                            Navigator.push(context, MaterialPageRoute(builder: (context) => SignIn())).then((value) => setState(() {}));
                          },
                              (String e)async{
                            hideLoading(context);
                            await showError(context, e);
                          });


                    },
                    child: Text(
                      "Выход",
                      style: Theme.of(context).textTheme.titleLarge!.copyWith(color: colors.error, fontWeight: FontWeight.w400),
                    ),

                  ),
                ),
              )



            ],
          ),
        ),
      )
    );
  }
}
