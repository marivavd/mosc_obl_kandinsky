import 'package:flutter/foundation.dart';
import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/main.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<List<Map<String, dynamic>>> getNewswithcoords()async{
  return await supabase.from('news').select().neq('geo_lat', 0).order('published_at', ascending: false);
}

Future<AuthResponse> signup(email, password)async{
  return await supabase.auth.signUp(
    email: email,
    password: password,
      data: {
        "fullname": "Ivanov Ivan Ivanoch",
        "birthday": '',
        "phone": '',
        "avatar": null,
        "platforms": []
      }

  );
}

Future<AuthResponse> signin(email, password)async{
  return await supabase.auth.signInWithPassword(
    email: email,
    password: password,
  );
}
Future<void> logout()async{
  await supabase.auth.signOut();
}

Future<void> sendEmail(email)async{
  await supabase.auth.resetPasswordForEmail(email);
}

Future<void> changePass(password)async{
  final UserResponse res = await supabase.auth.updateUser(
    UserAttributes(
      password: password,
    ),
  );
}

Future<void> OTP(email, code)async{
  final AuthResponse res = await supabase.auth.verifyOTP(
    type: OtpType.email,
    token: code,
    email: email
  );
}

Map<String, dynamic> getProfileAttributes(){
  return supabase.auth.currentUser!.userMetadata!;
}

Future<dynamic> getMyavatar()async{
  if (supabase.auth.currentUser!.userMetadata!['avatar'] == null){
    return null;
  }
  var file = await supabase.storage.from('avatars').download(supabase.auth.currentUser!.userMetadata!['avatar']);
  return file;
}

Future<void> uploadAvatar(Uint8List avatar)async{
  var name = "${supabase.auth.currentUser!.id}.png";
  await supabase.storage.from('avatars').uploadBinary(name, avatar);
  await supabase.auth.updateUser(
    UserAttributes(
      data: {
        'avatar': name
      }
    ),
  );

}

Future<void> updateProfile(ModelProfile profile)async{
  final UserResponse res = await supabase.auth.updateUser(
    UserAttributes(
      data: {
        'fullname': profile.fullname,
        'birthday': profile.birthday,
        'phone': profile.phone
      }
    ),
  );
}

Future<void> removeAvatar()async{
  await supabase.storage.from('avatars').remove(["${supabase.auth.currentUser!.id}.png"]);
  await supabase.auth.updateUser(
    UserAttributes(
        data: {
          'avatar': null
        }
    ),
  );
}

Future<UserResponse> savePlatforms(List<ModelPlatform> platforms) async {

  return await supabase.auth.updateUser(
      UserAttributes(
          data: {
            "platforms": platforms.map(
                    (e) => e.toJson()
            ).toList()
          }
      )
  );
}
List<ModelPlatform> loadPlatforms() {
  List<dynamic> shortData = supabase.auth.currentUser!.userMetadata!["platforms"];

  List<ModelPlatform> platforms = shortData.map((e) =>
      ModelPlatform.fromJson(e)
  ).toList();

  return platforms;
}
Future<List<ModelPlatform>> getAllPlatforms() async {
  var response = await supabase
      .from("platforms")
      .select();
  return response.map(
          (Map<String, dynamic> e) => ModelPlatform.fromJson(e)
  ).toList();
}
Future<List<Map<String, dynamic>>> get_news()async{
  return await supabase.from('news').select().order('published_at', ascending: false);

}

void subsribeNews(callback){
  supabase
      .channel("changes")
      .onPostgresChanges(
      event: PostgresChangeEvent.insert,
      schema: "public",
      table: "news",
      callback: (payload) {
        callback(payload.newRecord);
      }
  )
      .subscribe();
}


Future<Map<String, dynamic>> find_platform(String id)async{
  final data = await supabase
      .from('platforms')
      .select()
      .eq('id', id).single();
  return data;
}
