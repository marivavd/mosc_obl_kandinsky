import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';

class ModelProfile{
  String fullname;
  var avatar;
  String phone;
  String birthday;
  List<ModelPlatform> platforms;

  ModelProfile({required this.fullname, required this.phone, this.avatar, required this.birthday, required this.platforms});

}