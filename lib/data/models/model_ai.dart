/// YApi QuickType插件生成，具体参考文档:https://plugins.jetbrains.com/plugin/18847-yapi-quicktype/documentation

import 'dart:convert';

ModelAi modelAiFromJson(String str) => ModelAi.fromJson(json.decode(str));

String modelAiToJson(ModelAi data) => json.encode(data.toJson());

class ModelAi {
    ModelAi({
        required this.name,
        required this.id,
        required this.type,
        required this.version,
    });

    String name;
    int id;
    String type;
    double version;

    factory ModelAi.fromJson(Map<dynamic, dynamic> json) => ModelAi(
        name: json["name"],
        id: json["id"],
        type: json["type"],
        version: json["version"]?.toDouble(),
    );

    Map<dynamic, dynamic> toJson() => {
        "name": name,
        "id": id,
        "type": type,
        "version": version,
    };
}
