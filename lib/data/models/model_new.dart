import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';

class ModelNews {
  final String id;
  final String title;
  final String text;
  final String media;
  final ModelPlatform platform;
  final String published;
  final String type_media;

  ModelNews({required this.id, required this.title, required this.text, required this.media, required this.published, required this.platform, required this.type_media});
}
