import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/kandinsky.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/map.dart';
import 'package:mosc_obl_kandinsky/presentation/pages/sign_in.dart';
import 'package:mosc_obl_kandinsky/presentation/theme/colors.dart';
import 'package:mosc_obl_kandinsky/presentation/theme/theme.dart';
import 'package:mosc_obl_kandinsky/presentation/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<void> main() async {
  await Supabase.initialize(
    url: 'https://vmvbsgflwabnyuxsxxoa.supabase.co',
    anonKey: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InZtdmJzZ2Zsd2Fibnl1eHN4eG9hIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTE0NDgxNzgsImV4cCI6MjAyNzAyNDE3OH0.UM50aZasU0tmp2sa77T8ee2tqq_mYNW7dmHyo8jn6xo',
  );

  runApp(MyApp());
}

// Get a reference your Supabase client
final supabase = Supabase.instance.client;

class MyApp extends StatefulWidget {
  MyApp({super.key});
  bool isLight = true;

  void changeDiffTheme(BuildContext context){
    isLight = !isLight;
    context.findAncestorStateOfType<MyAppState>()?.onchangeTheme();
  }

  ColorsApp getColors(BuildContext context){
    return (isLight)?lightcolors:darkcolors;
  }

  ThemeData getCurrentTheme(){
    return (isLight)?light:dark;
  }
  static MyApp of(BuildContext context) {
    return context.findAncestorWidgetOfExactType<MyApp>()!;
  }


  @override
  State<MyApp> createState() => MyAppState();




}

class MyAppState extends State<MyApp>{
  final connectivity = Connectivity();
  late StreamSubscription<List<ConnectivityResult>> connectivityStream;

  void onchangeTheme(){
    setState(() {

    });
  }
  @override
  void initState() {
    super.initState();
    connectivityStream = connectivity.onConnectivityChanged.listen((event) {
      if (event == ConnectivityResult.none){
        showError(context, "Network crashed");
      }
    });

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    connectivityStream.cancel();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: widget.getCurrentTheme(),
      home: SignIn(),
    );
  }
}

