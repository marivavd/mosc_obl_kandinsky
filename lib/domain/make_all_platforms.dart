import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/data/models/model_platform.dart';
import 'package:mosc_obl_kandinsky/data/repository/requests.dart';

List<ModelPlatform> choose_platform(List<ModelPlatform> my_platforms, List<ModelPlatform> all_platforms){


  for (int j = 0; j < my_platforms.length; j++){
    for (int i=0; i < all_platforms.length;  i++){
      if (all_platforms[i].id == my_platforms[j].id){
        all_platforms[i].choosen = true;
        break;
      }
    }
  }
  return all_platforms;
}


Future<List<ModelPlatform>> make_all_platforms(List<ModelPlatform> my_platforms)async{
  List<ModelPlatform> all_platforms = [];
  all_platforms =  await getAllPlatforms();
  all_platforms = choose_platform(my_platforms, all_platforms);
  return all_platforms;
}

void work_with_platforms(List<ModelPlatform> all_platforms, index){
  all_platforms[index].choosen = !all_platforms[index].choosen;
}

List<ModelPlatform> choose_my_platforms(List<ModelPlatform> all_platforms){

  List<ModelPlatform> sp = [];
  for (int i=0; i < all_platforms.length;  i++){
    if (all_platforms[i].choosen)
      sp.add(all_platforms[i]);
  }

  return sp;
}

List<TextEditingController> make_controllers(List<ModelPlatform> platforms){
  List<TextEditingController> sp = [];
  for (int i=0; i< platforms.length; i++){
    sp.add(TextEditingController(text: platforms[i].channels));
  }
  return sp;
}
