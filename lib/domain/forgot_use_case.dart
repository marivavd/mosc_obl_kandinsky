import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class ForgotUseCase{
  Future<void> pressButton(
      String email,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestForgot() async{
      await sendEmail(email);
    }
    await requests(requestForgot, onResponse, onError);
  }
}