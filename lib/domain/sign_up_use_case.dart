import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class SignUpUseCase{
  Future<void> pressButton(
      String email,
      String password,
      String confirmPassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmPassword){
      onError('Passwords dont match');
    }
    else{
      requestSignUp() async{
        await signup(email, password);
      }
      await requests(requestSignUp, onResponse, onError);
    }
  }
}