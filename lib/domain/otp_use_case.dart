import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class OTPUseCase{
  Future<void> pressButton(
      String email,
      String code,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
      requestOTP() async{
        await OTP(email, code);
      }
      await requests(requestOTP, onResponse, onError);
    }
}