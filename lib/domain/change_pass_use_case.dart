import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class ChangePassUseCase{
  Future<void> pressButton(
      String password,
      String confirmPassword,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    if (password != confirmPassword){
      onError('Passwords dont match');
    }
    else{
      requestChangePass() async{
        await changePass(password);
      }
      await requests(requestChangePass, onResponse, onError);
    }
  }
}