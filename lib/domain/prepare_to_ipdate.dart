import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/data/repository/requests.dart';



Future<void> prepare_to_update({required ModelProfile profile, required TextEditingController fullname_controller, required TextEditingController phone_controller, required TextEditingController birthday_controller, required BuildContext context})async{
  profile.fullname = fullname_controller.text;
  profile.phone = phone_controller.text;
  profile.birthday = birthday_controller.text;
  check_avatar(profile.avatar);
  await updateProfile(profile);
}
void check_avatar(bytes){
  if (bytes != null){
    uploadAvatar(bytes);
  }
  else{
    removeAvatar();
  }
}

