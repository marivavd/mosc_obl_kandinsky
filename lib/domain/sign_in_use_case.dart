import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class SignInUseCase{
  Future<void> pressButton(
      String email,
      String password,
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
      requestSignIn() async{
        await signin(email, password);
      }
      await requests(requestSignIn, onResponse, onError);
  }
}