import 'package:dio/dio.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

Future<bool> request(
    Future<void> Function() func,
    Function(String) onError
    ) async {
  try{
    await func();
    return true;
  } on DioException catch (e){
    try{
      if (e.response?.data != null){
        onError('Не найдено');
      }
      return false;
    }catch(_){
      var code = e.response?.statusCode;
      if (e.response != null) {
        if (e.response!.statusCode != null){
          onError('Не найдено');
          return false;
        }else{
          onError(
              "STATUS: $code\n"
                  "MESSAGE: ${e.response?.statusMessage}"
          );
          return false;
        }
      } else {
        onError("Ошибка при отправке запроса");
        return false;
      }
    }
  }catch (e) {
    onError(e.toString());
    return false;
  }
}


Future<void> requests<T>(
    Future<T> Function() request,
    Function(T) onResponse,
    Future<void> Function(String) onError
)async{
  try{
    var response = await request();
    onResponse(response);
  }
  on AuthException catch(e){
    onError(e.message);
  }
  on Exception catch(e){
    onError(e.toString());
  }
  catch(e){
    onError(e.toString());
  }

}
bool checkEmail(String email){
  return RegExp(r"^[0-9a-z]+@[a-z]+\.[a-z]{2,}$").hasMatch(email);
}