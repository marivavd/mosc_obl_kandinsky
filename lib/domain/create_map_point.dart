import 'package:mosc_obl_kandinsky/data/models/map_point.dart';
import 'package:mosc_obl_kandinsky/data/repository/requests.dart';

Future<List<MapPoint>> createMapPoint()async{
  final data = await getNewswithcoords();
  List<MapPoint> result = [];
  for (int i =0; i <data.length; i++){
    result.add(MapPoint(name: data[i]['id'], latitude: double.tryParse(data[i]['geo_lat'].toString())??0.0, longitude: double.tryParse(data[i]['geo_long'].toString())??0.0));
  }
  return result;
}