import 'package:mosc_obl_kandinsky/data/models/model_profile.dart';
import 'package:mosc_obl_kandinsky/data/repository/requests.dart';

Future<ModelProfile> createProfile() async {
  final data = getProfileAttributes();
  return ModelProfile(fullname: data['fullname'], phone: data['phone'], birthday: data['birthday'], avatar: await getMyavatar(), platforms: loadPlatforms());
}