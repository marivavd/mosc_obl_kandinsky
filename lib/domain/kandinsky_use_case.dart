import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:mosc_obl_kandinsky/data/models/model_ai.dart';
import 'package:mosc_obl_kandinsky/data/models/model_generate_request.dart';
import 'package:mosc_obl_kandinsky/data/models/model_generation.dart';
import 'package:mosc_obl_kandinsky/data/models/model_style.dart';
import 'package:mosc_obl_kandinsky/data/repository/kand_request.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class KandinskyUseCase{
  bool Valid(
      String text,
      ModelStyle style,
      ModelAi modelai,
  {
    required Function(String) onNotValid
  }
      ){
    if (text.isEmpty){
      onNotValid("Введите своё имя");
      return false;
    }
    return true;
  }

  Future<void> pressButton(
      double ratio,
      String text,
      ModelAi? modelAI,
      ModelStyle? style,
      {
        required Function(String uuid) onInit,
        required Function(String uuid) onCheckStatus,
        required Function(Uint8List bytes) onDone,
        required Function(String uuid) onCensured,
        required Function(String error) onError
      }
      ) async {
    bool isValid = Valid(text, style!, modelAI!, onNotValid: onError);
    if (!isValid){
      return;
    }

    ModelGenerateRequest modelGenerateRequest = ModelGenerateRequest(
      generateParams: GenerateParams(query: text),
      width: 200,
      height: 200,
      negativePromptUnclip: '',
      style: style.name,
      numImages: 1,
    );
    String? id;

    requestStartGeneration() async {
      await startGenerate(
          modelGenerateRequest,
          modelAI,
          onInitGenerate: (String uuid){
            id = uuid;
            onInit(uuid);
          }
      );
    }
    requestToListenStatusChanges() async {
      startListenCheckStatusGeneration(
          id!,
          onDone: onDone,
          onCensured: onCensured,
          onError: onError,
          onCheckStatus: onCheckStatus
      );
    }

    await request(requestStartGeneration, onError);
    if (id == null){
      return;
    }
    await request(requestToListenStatusChanges, onError);
  }

  Future<void> startListenCheckStatusGeneration(
      String id,
      {
        required Function(String uuid) onCheckStatus,
        required Function(Uint8List bytes) onDone,
        required Function(String uuid) onCensured,
        required Function(String error) onError
      }
      ) async {
    startDelayed(Future<void> Function() func) async {
      await Future.delayed(const Duration(seconds: 1));
      await func();
    }

    Future<void> iterationCheck() async {
      requestCheckGeneration() async {
        await checkGenerate(
            id,
            onDone: (ModelGeneration model){
              var bytes = base64Decode(model.images.first);
              if (model.censored ?? false){
                onCensured(id);
                return;
              }
              onDone(bytes);
            },
            onCheckStatus: (status){
              onCheckStatus(status);
              startDelayed(iterationCheck);
            },
            onError: (String e){onError(e);}
        );
      }
      request(requestCheckGeneration, onError);
    }

    startDelayed(iterationCheck);
  }

  Future<void> getStyles(
      Function(List<ModelStyle>) onGenerate,
      Function(String) onError
      ) async {
    requestFetchStyles() async {
      await fetchStyles(onGenerate);
    }
    request(requestFetchStyles, onError);
  }

  Future<void> getModelAI(
      Function(ModelAi) onResponse,
      Function(String) onError
      ) async {
    requestGetIdModelsAI() async {
      await getModelsAI(
            (models){
          onResponse(models.first);
        },
          (String e){onError(e);}
      );
    }

    await request(requestGetIdModelsAI, onError);
  }
}