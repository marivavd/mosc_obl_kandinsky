import 'package:mosc_obl_kandinsky/data/repository/requests.dart';
import 'package:mosc_obl_kandinsky/domain/utils.dart';

class LogOutUseCase{
  Future<void> pressButton(
      Function(void) onResponse,
      Future<void> Function(String) onError
      )async{
    requestLogOut() async{
      await logout();
    }
    await requests(requestLogOut, onResponse, onError);
  }
}